<?php

declare(strict_types=1);

namespace Drupal\Tests\config_help\Functional;

use Drupal\language\Entity\ConfigurableLanguage;
use Drupal\Tests\BrowserTestBase;

/**
 * Tests translation for help topics.
 *
 * @group help
 */
class HelpTopicTranslateTest extends BrowserTestBase {

  /**
   * Modules to enable.
   *
   * @var array
   */
  protected static $modules = [
    'filter',
    'help',
    'config_help',
    'config_help_test',
    'config_translation',
    'locale',
    'language',
    'file',
    'field',
  ];

  /**
   * User who can administer help topics, translate, and view them.
   *
   * @var \Drupal\user\UserInterface
   */
  protected $adminUser;

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * {@inheritdoc}
   */
  protected function setUp():void {
    parent::setUp();

    // Create users.
    $this->adminUser = $this->createUser([
      'access help pages',
      'administer config help',
      'use text format help',
      'translate configuration',
      'administer languages',
      'administer site configuration',
    ]);

    // Add a language.
    ConfigurableLanguage::createFromLangcode('es')->save();
  }

  /**
   * Logs in users, tests help translation.
   */
  public function testHelpTranslation() {
    $this->drupalLogin($this->adminUser);

    // Verify that the help topics admin page has translate links.
    $this->drupalGet('admin/config/development/config-help');
    $session = $this->assertSession();
    $session->linkExists('Translate');

    // Translate a topic.
    $es_body = 'This is the fake Spanish body';
    $es_title = 'This is the fake Spanish title';
    $this->drupalGet('admin/config/development/config-help/manage/help_test/translate');
    $this->clickLink('Add');
    $this->submitForm([
      'translation[config_names][config_help.topic.help_test][label]' => $es_title,
      'translation[config_names][config_help.topic.help_test][body][0][text]' => $es_body,
    ], 'Save translation');

    // Visit the page in English and verify.
    $this->drupalGet('admin/help/topic/config_help:help_test');
    $session = $this->assertSession();
    $session->pageTextContains('ABC Help Test module');
    $session->pageTextContains('This is a test.');
    $session->pageTextNotContains($es_title);
    $session->pageTextNotContains($es_body);

    // Visit the page in Spanish and verify.
    $this->drupalGet('es/admin/help/topic/config_help:help_test');
    $session = $this->assertSession();
    $session->pageTextNotContains('ABC Help Test module');
    $session->pageTextNotContains('This is a test.');
    $session->pageTextContains($es_title);
    $session->pageTextContains($es_body);

    // Add a new topic sourced in Spanish.
    $second_en_title = 'Second Test';
    $second_es_title = 'Segunda Prueba';
    $second_en_body = 'Second body';
    $second_es_body = 'Segunda cuerpo';

    $this->drupalGet('admin/config/development/config-help/add');
    $this->submitForm([
      'langcode' => 'es',
      'label' => $second_es_title,
      'id' => 'foo',
      'top_level' => TRUE,
      'body[value]' => $second_es_body,
    ], 'Save');

    // Translate it into English.
    $this->drupalGet('admin/config/development/config-help/manage/foo/translate');
    $this->clickLink('Add');
    $this->submitForm([
      'translation[config_names][config_help.topic.foo][label]' => $second_en_title,
      'translation[config_names][config_help.topic.foo][body][0][text]' => $second_en_body,
    ], 'Save translation');

    // Visit the page in English and verify.
    $this->drupalGet('admin/help/topic/config_help:foo');
    $session = $this->assertSession();
    $session->pageTextContains($second_en_title);
    $session->pageTextContains($second_en_body);
    $session->pageTextNotContains($second_es_title);
    $session->pageTextNotContains($second_es_body);

    // Visit the page in Spanish and verify.
    $this->drupalGet('es/admin/help/topic/config_help:foo');
    $session = $this->assertSession();
    $session->pageTextNotContains($second_en_title);
    $session->pageTextNotContains($second_en_body);
    $session->pageTextContains($second_es_title);
    $session->pageTextContains($second_es_body);

    // Visit the help landing page and verify correct title links are shown
    // in both languages.
    $this->drupalGet('admin/help');
    $session = $this->assertSession();
    $session->linkExists('ABC Help Test module');
    $session->linkNotExists($es_title);
    $session->linkExists($second_en_title);
    $session->linkNotExists($second_es_title);

    $this->drupalGet('es/admin/help');
    $session = $this->assertSession();
    $session->linkNotExists('ABC Help Test module');
    $session->linkExists($es_title);
    $session->linkNotExists($second_en_title);
    $session->linkExists($second_es_title);
  }

}
