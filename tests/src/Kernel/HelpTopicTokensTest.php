<?php

declare(strict_types=1);

namespace Drupal\Tests\config_help\Kernel;

use Drupal\Core\Render\BubbleableMetadata;
use Drupal\Core\Url;
use Drupal\KernelTests\KernelTestBase;
use Drupal\language\Entity\ConfigurableLanguage;

/**
 * Tests token generation for help topics.
 *
 * @group help
 */
class HelpTopicTokensTest extends KernelTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'system',
    'help',
    'config_help',
    'config_help_test',
    'user',
    'filter',
    'language',
  ];

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    // Set up as multi-lingual with English and Spanish, so that we can
    // test for cache metadata from URL generation.
    $this->installConfig(['config_help', 'config_help_test', 'language']);
    $this->installEntitySchema('configurable_language');
    ConfigurableLanguage::create(['id' => 'es'])->save();
    $this
      ->config('language.negotiation')
      ->set('url.prefixes', [
        'en' => 'en',
        'es' => 'es',
      ])
      ->save();
    \Drupal::service('kernel')->rebuildContainer();
    \Drupal::service('router.builder')->rebuild();
  }

  /**
   * Tests that help topic tokens work.
   */
  public function testHelpTopicTokens() {
    // Verify a URL token for a help topic that is a plugin.
    $bubbleable_metadata = new BubbleableMetadata();
    $topic_id = 'config_help_test.foo';
    $text = 'This should <a href="[help_topic:url:' . $topic_id . ']">Link to help topic</a>';
    $replaced = \Drupal::token()->replace($text, [], [], $bubbleable_metadata);
    $this->assertTrue(strpos($replaced, '<a href="' . Url::fromRoute('help.help_topic', ['id' => $topic_id])->toString() . '"') !== FALSE, 'Topic URL token replacement worked');

    // Check for cache metadata. It should have a cache context for language
    // negotiation. As this is a plugin, there is no cache tag for the topic.
    $contexts = $bubbleable_metadata->getCacheContexts();
    $this->assertTrue(in_array('languages:language_url', $contexts), 'Language negotiation cache context was added');

    // Verify correct URL if we tell the Token system to use Spanish.
    $bubbleable_metadata = new BubbleableMetadata();
    $replaced = \Drupal::token()->replace($text, [], ['langcode' => 'es'], $bubbleable_metadata);
    $spanish = \Drupal::languageManager()->getLanguage('es');
    $this->assertTrue(str_contains($replaced, '<a href="' . Url::fromRoute('help.help_topic', ['id' => $topic_id], ['language' => $spanish])
      ->toString() . '"'), 'Topic URL token replacement worked in Spanish');

    // Verify that replacement does not happen for topic that does not exist.
    $text = 'This should <a href="[help_topic:url:nonexistent]">Not link to help topic</a>';
    $replaced = \Drupal::token()->replace($text);
    $this->assertTrue(str_contains($replaced, '[help_topic:url:nonexistent]'), 'Nonexistent help topic did not get replaced');

    // Verify a URL for a help topic that is an entity.
    $topic_plugin_id = 'config_help:help_test';
    $topic_entity_id = 'help_test';
    $bubbleable_metadata = new BubbleableMetadata();
    $text = 'This should <a href="[help_topic:url:' . $topic_plugin_id . ']">Link to entity help topic</a>';
    $replaced = \Drupal::token()->replace($text, [], [], $bubbleable_metadata);
    $this->assertTrue(str_contains($replaced, '<a href="' . Url::fromRoute('help.help_topic', ['id' => $topic_plugin_id])
      ->toString() . '"'), 'Entity topic URL token replacement worked');

    // Check for cache metadata. It should have a cache context for language
    // negotiation, and a tag for the entity.
    $tags = $bubbleable_metadata->getCacheTags();
    $contexts = $bubbleable_metadata->getCacheContexts();
    $this->assertTrue(in_array('config:config_help.topic.' . $topic_entity_id, $tags), 'Cache tag for the linked topic was added');
    $this->assertTrue(in_array('languages:language_url', $contexts), 'Language negotiation cache context was added');
  }

  /**
   * Tests that route tokens work.
   */
  public function testRouteTokens() {
    // Verify correct URL for a system route.
    $bubbleable_metadata = new BubbleableMetadata();
    $text = 'This should <a href="[route:url:system.admin]">Link to admin</a>';
    $replaced = \Drupal::token()->replace($text, [], [], $bubbleable_metadata);
    $this->assertTrue(str_contains($replaced, '<a href="' . Url::fromRoute('system.admin')
      ->toString() . '"'), 'Route token was replaced correctly');

    // Check for cache metadata for language negotiation.
    $contexts = $bubbleable_metadata->getCacheContexts();
    $this->assertTrue(in_array('languages:language_url', $contexts), 'Language negotiation cache context was added');

    // Verify there is no replacement for an invalid route.
    $text = 'This should <a href="[route:url:system.nonexistent]">Not link to admin</a>';
    $replaced = \Drupal::token()->replace($text);
    $this->assertTrue(str_contains($replaced, '[route:url:system.nonexistent]'), 'Nonexistent route was not replaced');
  }

}
