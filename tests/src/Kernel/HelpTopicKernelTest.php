<?php

declare(strict_types=1);

namespace Drupal\Tests\config_help\Kernel;

use Drupal\config_help\Entity\HelpTopic;
use Drupal\Core\Serialization\Yaml;
use Drupal\KernelTests\KernelTestBase;

/**
 * Tests some core functionality of the Help Topic entity.
 *
 * @group help
 */
class HelpTopicKernelTest extends KernelTestBase {

  /**
   * Modules to enable.
   *
   * @var array
   */
  protected static $modules = [
    'system',
    'help',
    'filter',
    'config_help',
  ];

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();
    $this->installConfig(['config_help']);
  }

  /**
   * Tests the help topic body get and set functions.
   */
  public function testTopicBodyGetSet() {
    $topic = HelpTopic::create();

    // Create a body value with lots of different HTML tags, some entities, and
    // some whitespace of various types between tags, and some text between
    // tags.
    $body1 = '<p class="testing123" id="something" style="color:red;">A paragraph with attributes</p>';
    // phpcs:disable Drupal.Strings.UnnecessaryStringConcat.Found
    $body2 = '<h2>A heading with an &amp; entity in it</h2>' .
      '   some between-tag text with whitespace and containing a tag as entity &lt;p&gt; ' .
      '<h3>A sub-heading</h3>' .
      '<ul><li>Bullet 1</li><li>Bullet 2</li></ul>' .
      '<ol><li>Number 1</li><li>Number 2</li></ol>' .
      '<table><thead><tr><th>Col 1</th><th>Col 2</th></tr></thead>' .
      '<tbody><tr><td>Data 1</td><td>Data 2</td></tr></tbody></table>' .
      '<dl><dt>  Item 1 with spaces  </dt><dd>Definition 1</dd><dt>Item 2 &gt;</dt><dd>Definition 2</dd></dl>';
    $body = $body1 . "\t\n\r\v &#13; " . $body2;
    $body_expected = $body1 . $body2;

    // Make sure after set/get, the body is unchanged except expected
    // whitespace deletions.
    $topic->setBody($body);
    $body_out = $topic->getBody($body);
    $this->assertSame($body_expected, $body_out);

    // Make sure that if we strip out tags, all remaining text is in the 'text'
    // parts of the chunked body array.
    $body_plain = strip_tags($body_expected);
    $body_chunked = $topic->toArray()['body'];
    $chunked_text = '';
    foreach ($body_chunked as $item) {
      $prefix = strip_tags($item['prefix_tags']);
      $suffix = strip_tags($item['suffix_tags']);
      // Prefix and suffix should only consist of tags.
      $this->assertEmpty($prefix);
      $this->assertEmpty($suffix);
      // We shouldn't have any text items that are pure whitespace.
      $this->assertNotEmpty(trim($item['text']));
      $chunked_text .= $item['text'];
    }
    $this->assertSame($body_plain, $chunked_text);
  }

  /**
   * Tests export and import of help topics, and topic create from array.
   */
  public function testTopicExportImport() {
    // Create a help topic entity.
    $values = [
      'id' => 'foo',
      'label' => 'Foo',
      'body' => [
        [
          'text' => 'Greetings',
          'prefix_tags' => '<h3>',
          'suffix_tags' => '</h3>',
        ],
        [
          'text' => 'Hello, world!',
          'prefix_tags' => '<p>',
          'suffix_tags' => '</p>',
        ],
      ],
      'body_format' => 'help',
      'top_level' => TRUE,
      'related' => ['help_system_building'],
    ];

    /** @var \Drupal\config_help\Entity\HelpTopicInterface $foo */
    $foo = HelpTopic::create($values);
    // Dependencies are only calculated when entity is saved, so force
    // calculation.
    $foo->calculateDependencies();

    // Export and import the topic.
    $foo_export = Yaml::encode($foo->toArray());
    $bar = HelpTopic::create(Yaml::decode($foo_export));
    $bar->calculateDependencies();

    // Verify that everything is OK.
    $to_check = [
      // This is an array of method name => component in $values, except body,
      // which is checked in the other test method.
      'id' => 'id',
      'label' => 'label',
      'getBody' => FALSE,
      'getBodyFormat' => 'body_format',
      'isTopLevel' => 'top_level',
      'getRelated' => 'related',
    ];

    // Verify that the initial create got the right values, and that after
    // export/import, the values are the same.
    foreach ($to_check as $method => $component) {
      if ($component) {
        $this->assertSame(call_user_func([$foo, $method]), $values[$component], 'Data for ' . $component . ' is the same as method ' . $method);
      }
      $this->assertSame(call_user_func([$foo, $method]), call_user_func([$bar, $method]), 'Method ' . $method . ' is the same before and after export/import');
    }

    // Verify that the body format dependency is there.
    $after_dependencies = $bar->getDependencies();
    $this->assertSame($after_dependencies['config'][0], 'filter.format.help');
  }

}
