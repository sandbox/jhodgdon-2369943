<?php

/**
 * @file
 * Builds placeholder replacement tokens for help topics and routes.
 */

use Drupal\Core\Render\BubbleableMetadata;
use Drupal\Core\Url;

/**
 * Implements hook_token_info().
 */
function config_help_token_info() {
  $types['route'] = [
    'name' => t("Route information"),
    'description' => t("Tokens based on route names."),
  ];

  $routes = [];
  $routes['url'] = [
    'name' => t('URL'),
    'description' => t('The URL to the route. Provide the route name as route:url:ROUTE_NAME'),
  ];

  $types['help_topic'] = [
    'name' => t("Help Topics"),
    'description' => t("Tokens for help topics."),
  ];

  $topics = [];
  $topics['url'] = [
    'name' => t('URL'),
    'description' => t('The URL to the topic. Provide the topic machine name as config_help:url:MACHINE_NAME'),
  ];

  return [
    'types' => $types,
    'tokens' => [
      'help_topic' => $topics,
      'route' => $routes,
    ],
  ];
}

/**
 * Implements hook_tokens().
 */
function config_help_tokens($type, $tokens, array $data, array $options, BubbleableMetadata $bubbleable_metadata) {
  $replacements = [];
  $token_service = \Drupal::token();
  /** @var \Drupal\help\HelpTopicPluginManagerInterface $plugin_manager */
  $plugin_manager = \Drupal::service('plugin.manager.help_topic');

  // Our tokens generate URLs, which depend on language. See if a language is
  // passed in; if not, we will let the URL system use its own defaults.
  $url_options = [];
  if (isset($options['langcode'])) {
    $language_manager = \Drupal::languageManager();
    $language = $language_manager->getLanguage($options['langcode']);
    if ($language) {
      $url_options['language'] = $language;
    }
  }

  if ($type == 'help_topic') {
    $topics = $token_service->findWithPrefix($tokens, 'url');
    foreach ($topics as $name => $original) {
      /** @var \Drupal\help\HelpTopicPluginInterface $topic */
      if ($plugin_manager->hasDefinition($name) && $topic = $plugin_manager->createInstance($name, [])) {
        $url = $topic->toUrl($url_options)->toString(TRUE);
        $replacements[$original] = $url->getGeneratedUrl();
        $bubbleable_metadata
          ->addCacheableDependency($url)
          ->addCacheableDependency($topic);
      }
    }
  }
  elseif ($type == 'route') {
    $routes = $token_service->findWithPrefix($tokens, 'url');
    foreach ($routes as $route_name => $original) {
      try {
        $url = Url::fromRoute($route_name, [], $url_options)->toString(TRUE);
        $replacements[$original] = $url->getGeneratedUrl();
        $bubbleable_metadata->addCacheableDependency($url);
      }
      catch (\Exception $e) {
        // Invalid route or missing parameters or something like that.
        // Do nothing.
      }
    }
  }

  return $replacements;
}
