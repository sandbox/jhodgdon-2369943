<?php

declare(strict_types=1);

namespace Drupal\config_help\Controller;

use Drupal\Component\Render\HtmlEscapedText;
use Drupal\Component\Utility\Tags;
use Drupal\Core\Controller\ControllerBase;
use Drupal\help\HelpTopicPluginManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * Autocomplete controller routines for configurable help edit pages.
 */
class AutocompleteController extends ControllerBase {

  /**
   * The help topic plugin manager.
   */
  protected HelpTopicPluginManagerInterface $pluginManager;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    $instance = parent::create($container);
    $instance->pluginManager = $container->get('plugin.manager.help_topic');
    return $instance;
  }

  /**
   * Retrieves suggestions for help topic autocomplete.
   *
   * The autocomplete suggestions search for matches by topic title and machine
   * name, and are returned in a JSON response for use in an edit form field.
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   The request object.
   *
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   *   A JSON response containing the autocomplete suggestions.
   */
  public function topicAutocomplete(Request $request) {
    $matches = [];
    $count = 0;
    if ($input = $this->getUserInput($request)) {
      $topics = $this->findTopicMatches($input);
      if (!empty($topics)) {
        foreach ($topics as $title => $id) {
          $matches[] = $this->getMatch($id, $title);
          $count++;
          if ($count >= 10) {
            break;
          }
        }
      }
    }

    return new JsonResponse($matches);
  }

  /**
   * Returns the last typed tag from user input passed to autocomplete.
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   The request object.
   *
   * @return string
   *   The last tag.
   */
  protected function getUserInput(Request $request) {
    $input = $request->query->get('q');
    if (!$input) {
      return '';
    }
    $input = Tags::explode($input);
    return mb_strtolower(array_pop($input));
  }

  /**
   * Returns a list of topic matches for the given text.
   *
   * @param string $text
   *   Text to match.
   *
   * @return string[]
   *   Array of matching topic IDs, keyed by topic title, where $text matches
   *   a substring of the title (in its base language) or ID, with case-
   *   insensitive matching. Returned in alphabetic order by title.
   */
  protected function findTopicMatches($text) {
    $topics = [];
    foreach ($this->pluginManager->getDefinitions() as $definition) {
      $label = (string) $definition['label'];
      if ((stripos($definition['id'], $text) !== FALSE) ||
        (stripos($label, $text) !== FALSE)) {
        $topics[$label] = $definition['id'];
      }
    }

    ksort($topics);
    return $topics;
  }

  /**
   * Builds the structure to display in an autocomplete dropdown.
   *
   * @param string $value
   *   Machine name to display and use as the value.
   * @param string $label
   *   Human-readable name to display.
   *
   * @return array
   *   An array of matched labels, in the format required by the Ajax
   *   autocomplete API (array('value' => $value, 'label' => $label)).
   */
  protected function getMatch($value, $label) {
    return [
      'value' => $value,
      'label' => new HtmlEscapedText("$label ($value)"),
    ];
  }

}
