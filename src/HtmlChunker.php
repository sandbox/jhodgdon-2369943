<?php

declare(strict_types=1);

namespace Drupal\config_help;

use Drupal\Component\Utility\Html;

/**
 * Utility for separating HTML into paragraph-sized chunks, and rejoining it.
 */
class HtmlChunker {

  /**
   * Breaks HTML into paragraph-sized chunks.
   *
   * @param string $html
   *   HTML to break into chunks.
   *
   * @return array|false
   *   FALSE if the HTML is invalid, and an empty array if $html is empty.
   *   Otherwise, an array of paragraph-sized chunks. Each element of the array
   *   is an array with elements:
   *   - text: Text content of the chunk, which may contain HTML.
   *   - prefix_tags: HTML tags that go before the text content.
   *   - suffix_tags: HTML tags that go after the text content.
   *
   * @see \Drupal\help\HtmlChunker::joinChunks()
   */
  public static function chunkHtml($html) {
    $dom = Html::load($html);
    if (!$dom) {
      return FALSE;
    }

    $chunks = [];
    $body_node = $dom->getElementsByTagName('body')->item(0);
    if ($body_node) {
      $chunks = self::chunkDomNode($dom, $body_node);
    }
    return $chunks;
  }

  /**
   * Joins chunks into an HTML string.
   *
   * @param array $chunks
   *   Array of chunks, each of which is an array with elements:
   *   - text: Text content of the chunk, which may contain HTML.
   *   - prefix_tags: HTML tags that go before the text content.
   *   - suffix_tags: HTML tags that go after the text content.
   *
   * @return string
   *   HTML string containing all of the chunks.
   *
   * @see \Drupal\help\HtmlChunker::chunkHtml()
   */
  public static function joinChunks($chunks) {
    $text = '';
    foreach ($chunks as $chunk) {
      $text .= $chunk['prefix_tags'] . $chunk['text'] . $chunk['suffix_tags'];
    }
    return $text;
  }

  /**
   * Splits a DOM node recursively into chunks.
   *
   * @param \DOMDocument $dom
   *   The DOM document this came from.
   * @param \DOMNode $node
   *   DOM node to split into chunks.
   * @param string $prefix
   *   (optional) Prefix tags to put on first chunk.
   * @param string $suffix
   *   (optional) Suffix tags to put on last chunk.
   *
   * @return array
   *   Array of chunks from $node. Each chunk is an array with elements:
   *   - text: Text content of the chunk, which may contain HTML.
   *   - prefix_tags: HTML tags that go before the text content.
   *   - suffix_tags: HTML tags that go after the text content.
   *   The intent is that if you concatenate all the chunks' prefix_tags,
   *   text, and suffix_tags, you will end up with the original HTML.
   */
  protected static function chunkDomNode(\DOMDocument $dom, \DOMNode $node, $prefix = '', $suffix = '') {

    // These HTML tags generate an automatic chunk.
    $chunk_tags = [
      'p',
      'h1',
      'h2',
      'h3',
      'h4',
      'h5',
      'h6',
      'li',
      'dt',
      'dd',
      'code',
      'pre',
      'blockquote',
      'td',
      'th',
      'caption',
    ];

    $chunks = [];
    foreach ($node->childNodes as $child) {
      switch ($child->nodeType) {
        case XML_ELEMENT_NODE:
          if (!$child->hasChildNodes()) {
            // It's an empty element, so just add it to the prefix for the
            // next chunk.
            $prefix .= trim($dom->saveXML($child));
          }
          else {
            $open_tag = '<' . $child->tagName;
            if ($child->hasAttributes()) {
              foreach ($child->attributes as $attr) {
                $open_tag .= $dom->saveXML($attr);
              }
            }
            $open_tag .= '>';
            $close_tag = '</' . $child->tagName . '>';

            if (in_array($child->tagName, $chunk_tags)) {
              // Don't go deeper, just save everything this node contains
              // as one chunk.
              $text = '';
              foreach ($child->childNodes as $inner) {
                $text .= $dom->saveXML($inner);
              }
              $chunks[] = [
                'text' => $text,
                'prefix_tags' => $prefix . $open_tag,
                'suffix_tags' => $close_tag,
              ];
              $prefix = '';
            }
            else {
              // Recursively generate chunks from this node's children.
              $new_chunks = self::chunkDomNode($dom, $child, $prefix . $open_tag, $close_tag);
              $chunks = array_merge($chunks, $new_chunks);
              $prefix = '';
            }
          }
          break;

        case XML_TEXT_NODE:
          // Add non-empty text nodes as their own chunks. Some text nodes
          // are purely inter-tag whitespace; leave those out.
          $text = $dom->saveXML($child);
          // For some reason, these text nodes ar often containing the
          // carriage return character as an entity, so decode that
          // specifically.
          $text = str_replace('&#13;', "\r", $text);
          if (trim($text)) {
            $chunks[] = [
              'text' => $text,
              'prefix_tags' => $prefix,
              'suffix_tags' => '',
            ];
            $prefix = '';
          }
          break;

        default:
          // For nodes that are anything except elements or text, just add
          // them to the prefix we are working on. These are things like HTML
          // comments, for example.
          $prefix .= trim($dom->saveXML($child));
          break;
      }
    }

    // If we have prefix left over, add it as a chunk.
    if ($prefix) {
      $chunks[] = [
        'text' => '',
        'prefix_tags' => $prefix,
        'suffix_tags' => '',
      ];
    }

    // If we have a suffix left over, add it to the last chunk.
    if ($suffix) {
      if (!$chunks) {
        $chunks[] = [
          'text' => '',
          'prefix_tags' => '',
          'suffix_tags' => '',
        ];
      }
      $chunks[count($chunks) - 1]['suffix_tags'] .= $suffix;
    }

    return $chunks;
  }

}
