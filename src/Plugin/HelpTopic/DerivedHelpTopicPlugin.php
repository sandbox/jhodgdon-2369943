<?php

declare(strict_types=1);

namespace Drupal\config_help\Plugin\HelpTopic;

use Drupal\Component\Plugin\Exception\PluginException;
use Drupal\config_help\Entity\HelpTopicInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\help\HelpTopicPluginBase;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Wraps the help topic entity into a help topic plugin.
 *
 * @phpstan-ignore-next-line
 */
class DerivedHelpTopicPlugin extends HelpTopicPluginBase implements ContainerFactoryPluginInterface {

  /**
   * The entity this plugin is wrapping.
   */
  protected HelpTopicInterface $entity;

  /**
   * The entity type manager.
   */
  protected EntityTypeManagerInterface $entityTypeManager;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    $instance = new self($configuration, $plugin_id, $plugin_definition);
    $instance->entityTypeManager = $container->get('entity_type.manager');
    if (!isset($plugin_definition['metadata']['entity_id'])) {
      throw new PluginException('Missing entity ID in plugin definition');
    }
    $help_topic_storage = $instance->entityTypeManager->getStorage('config_help');
    $entities = $help_topic_storage->loadMultiple([$plugin_definition['metadata']['entity_id']]);
    if (!$entities || !count($entities)) {
      throw new PluginException('Invalid entity ID in plugin definition');
    }
    $entity = reset($entities);
    $instance->entity = clone($entity);

    // The plugin manager does some manipulation on the related field after
    // loading definitions. So, override with what is in the plugin definition.
    $instance->entity->setRelated($plugin_definition['related']);
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function getLabel() {
    return $this->entity->label();
  }

  /**
   * {@inheritdoc}
   */
  public function getBody() {
    return $this->entityTypeManager->getViewBuilder('config_help')->view($this->entity, 'default');
  }

  /**
   * {@inheritdoc}
   */
  public function getBodyFormat() {
    return $this->entity->getBodyFormat();
  }

  /**
   * {@inheritdoc}
   */
  public function isTopLevel() {
    return $this->entity->isTopLevel();
  }

  /**
   * {@inheritdoc}
   */
  public function getRelated() {
    return $this->entity->getRelated();
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheMaxAge() {
    return $this->entity->getCacheMaxAge();
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheContexts() {
    return $this->entity->getCacheContexts();
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheTags() {
    return $this->entity->getCacheTags();
  }

}
